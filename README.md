Bevan's vim setup.

Install by cloning this repo into ~/.vim using the following command which
will include all the submodules:

 git clone --recursive git@bitbucket.org:bevan_wishart/vim_config.git

I have this vim configure to use pathogen to work with a number
of plugins.

phpctags.

I have integrated phpctags, I have php ctags in here as a submodule.


This vim will automatically update your vim tags file for you, rather
quickly and in the background, so as not to disturb your vim session.

To set this up, you can create a file name .project_vim in the root
of your project.

You can add your own settings or overrides of the .vimrc file by
adding them here. Even if empty, you will need this file in the roo
of your project, as it's location is used to determine the projects root.


This readme needs work. :)


New Submodules

When doing a pull, check for new submodules with
 git submodule update --init --recursive

Setting up phpctags.
AFter setting up submodules do.
 cd phpctags
 make


Setting up integrated PHP documentation

First: Install the php documentation in manpage format.
sudo pear channel-update
sudo pear install doc.php.net/pman
