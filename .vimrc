" Following lines added by drush vimrc-install on Mon, 11 Jan 2016 03:16:42 +0000.
set nocompatible
execute pathogen#infect()
"Tell pathogen to automatically generate help tags.
call pathogen#helptags()

call pathogen#infect('/home/bevan/.drush/vimrc/bundle/{}')
call pathogen#infect('/home/bevan/.vim/bundle/{}')

let g:vdebug_options = {
\ 'server': '0.0.0.0',
\ 'port': '9000'
\}

" End of vimrc-install additions.
source $VIMRUNTIME/vimrc_example.vim

"shawncplus's phpcomplete
"pathogen --- package management (git clone plugins into pathogen/bundle)
"phpdocumenter (pdv) -- ^p documenter.. put params
"supertab -- code complete
"tagbar
"phpctags -- version from mr-russ (on github).

"Setup pathogen.
execute pathogen#infect()

set expandtab
set tabstop=2
set shiftwidth=2
set autoindent
set smartindent
set enc=utf8


"Change settings based on path.
function! SetupEnvironment()
  let l:path = expand('%:p')
  if l:path =~ '/var/www/moodle-site-oua'
    setlocal tabstop=4 shiftwidth=4
  endif
endfunction
autocmd! BufReadPost,BufNewFile * call SetupEnvironment()

filetype plugin indent on
filetype indent on

" Highlight redundant whitespaces and tabs.
highlight RedundantSpaces ctermbg=red guibg=red
match RedundantSpaces /\s\+$\| \+\ze\t\|\t/

let PHP_vintage_case_default_indent = 1

if has("autocmd")
  " Drupal *.module and *.install files.
  augroup module
    autocmd BufRead,BufNewFile *.module set filetype=php
    autocmd BufRead,BufNewFile *.install set filetype=php
    autocmd BufRead,BufNewFile *.test set filetype=php
    autocmd BufRead,BufNewFile *.class set filetype=php
  augroup END
endif

"if has("syntax")
"  syntax on
"  filetype on
"  au BufNewFile,BufRead current set filetype=tks
"endif

function! BufSel(pattern)
  let bufcount = bufnr("$")
  let currbufnr = 1
  let nummatches = 0
  let firstmatchingbufnr = 0
  while currbufnr <= bufcount
    if(bufexists(currbufnr))
      let currbufname = bufname(currbufnr)
      if(match(currbufname, a:pattern) > -1)
        echo currbufnr . ": ". bufname(currbufnr)
        let nummatches += 1
        let firstmatchingbufnr = currbufnr
      endif
    endif
    let currbufnr = currbufnr + 1
  endwhile
  if(nummatches == 1)
    execute ":buffer ". firstmatchingbufnr
  elseif(nummatches > 1)
    let desiredbufnr = input("Enter buffer number: ")
    if(strlen(desiredbufnr) != 0)
      execute ":buffer ". desiredbufnr
    endif
  else
    echo "No matching buffers"
  endif
endfunction

"Bind the BufSel() function to a user-command
command! -nargs=1 Bs :call BufSel("<args>")

"Bevans additions

map <F9> cs"'
:nnoremap buff :buffers<CR>:buffer<Space>

" change working dir to location of file in current buffer
set autochdir

" better tab completion
set wildmode=longest,list,full
set wildmenu

"clear drupal cache
map cc :!drush cc all<CR>
"look up passwords for current project in pview on turing
map pass :!clear<CR>:!pass<CR>
"look up PHP documentation for function under cursor
noremap <F8> :!gnome-open http://php.net/<cword><CR>;
"look up workrequest for WR# under cursor
noremap <F9> :!gnome-open https://wrms.catalyst.net.nz/wr.php?request_id=<cword><CR>;
"list the function definitions in current file
map func :!grep ^function.*{$ %\|less<CR>
map ws :%s/\s\+$//<cr>
map fn =expand("%:t:r")<CR>

map gb :execute('!git blame %\|grep -m1 ' . line('.') . '\)')<cr>
map gs :execute('!php ~/scripts/gs.php % ' . line('.'))<cr>

let g:pdv_template_dir = $HOME ."/.vim/bundle/pdv/templates_snip"
nnoremap <C-p> :call pdv#DocumentWithSnip()<CR>

map } :tag <C-R><C-W>

" Enable mouse support
:set mouse=a

function! SaveTasks()
  let l:fname = resolve(expand('%:t'))
  if l:fname =~ '^.*\.tks$'
    echo "TIMESHEET FILE.. commiting"
    !tks -c %
  endif

 
  if !empty(g:project_root)
    if g:project_root != $HOME
      let pidcmd = "/usr/bin/pgrep -f \"php /home/bevan/.vim/phpctags/phpctags\""
      silent execute "!" . pidcmd |redraw!
      "echom "Error: " . v:shell_error
      if v:shell_error == 1
        echom "Generating phpctags"
        let phpctagscommand = "!~/.vim/phpctags/phpctags --verbose=yes --extensions='.php .module .inc .install .test .profile .theme .class' --exclude=" . g:project_root . "/vendor --memory=1000M -R -f " . g:project_root . "/tags -C " . g:project_root . "/tag.cache " . g:project_root . " &>>/tmp/vim.log &"
        silent execute phpctagscommand |redraw!
      endif
    endif
  endif

endfunction

autocmd! BufWritePost * call SaveTasks()


"Set up vim refactoring
"@see https://github.com/vim-php/vim-php-refactoring
let g:php_refactor_command='php ~/.vim/php-refactoring-browser/refactor.phar'

let g:vdebug_features = { 'max_children': 128 }

"Find the .vim folder in our project
let project_vim = finddir(".vim", ".;")

"If we are in the root, finddir returnsa relative parth
"convert it to a full path.
if project_vim == ".vim"
  let project_vim = $PWD . "/" . project_vim
endif

"if we have a project vim, then load any project specific
"plugins.
if !empty(project_vim)
  execute pathogen#infect(project_vim . "/{}")
endif

"Set the project root to the parent of .vim
let project_root = fnamemodify(project_vim, ":h")

"Set the path to the project_root with recursive wild card.
" this way the :find command can search our project.
if !empty(project_root)
  let &path=project_root . "/**"
endif

"Use the tags file in the root of our project.
if !empty(project_root)
  let &tags=project_root . "/tags"
endif
